latexfile = online_laboratory

figures = social_pref2.eps social_pref_rep.eps how_to-004.eps trustOrig.eps

referencefile = references.bib

TEX = latex 

all : $(latexfile).pdf clean
.PHONY: all

$(latexfile).pdf : $(latexfile).ps 
	ps2pdf $(latexfile).ps $(latexfile).pdf

$(latexfile).ps : $(latexfile).dvi
	dvips $(latexfile)

$(latexfile).dvi : $(figures) $(latexfile).tex $(referencefile)
	$(TEX) $(latexfile)
	$(TEX) $(latexfile)
	bibtex $(latexfile)
	$(TEX) $(latexfile)
	$(TEX) $(latexfile)

clean: 
	rm *.out *.log *.blg *.bbl *.ps *.dvi *.aux
