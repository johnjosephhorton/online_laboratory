clear
insheet using "C:\Documents and Settings\Dave-O\My Documents\RESEARCH PROJECTS\sarah coakley\online experiments\20100203_all_data.txt"

sort treatment0neutral1religious2secu god_exist
by treatment0neutral1religious2secu god_exist : sum decision

sort treatment
by treatment: tabulate decision god_exist, chi2

sort god_exist
by god_exist: tabulate decision treatment if treatment<2, chi2

gen old=0
replace old=1 if treatment>2

gen relig=0
replace relig=1 if treatment==1 | treatment==4

gen sec=0
replace sec=1 if treatment==2

gen I_g_t = god_exist * treatment
gen I_g_r = god_exist * relig
gen I_g_s = god_exist * sec

// religious prime works
logit decision relig god_ I_g_r if treatment==0 | treatment==1, r
logit decision relig age gender us christian god_ I_g_r if treatment==0 | treatment==1, r
// esp well using the original religion data only
logit decision relig age gender us christian god_ I_g_r if treatment==0 | treatment==4, r
// or both together
logit decision relig age gender us christian god_ I_g_r if treatment==0 | treatment==4 | treatment==1, r

// with neutral prime, gods give less; with religious prime, they give more
sort relig
by relig: tabulate decision god_exist if treatment~=3 & treatment~=2, chi2
by relig: ttest decision if treatment~=3 & treatment~=2, by(god_exist)

sort god_e
by god_: tabulate decision relig if treatment~=3 & treatment~=2, chi2
by god_: ttest decision if treatment~=3 & treatment~=2, by(relig)



// secular prime doesnt do anything
logit decision sec age gender us christian god_ if treatment==0 | treatment==2, r
logit decision sec age gender us christian god_ I_g_s if treatment==0 | treatment==2, r

