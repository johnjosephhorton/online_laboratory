clear
insheet using "C:\Documents and Settings\Dave-O\My Documents\RESEARCH PROJECTS\john horton\My Dropbox\how_to\social_preference_game_component\20100201_all_data.txt"

sort treatment0neutral1religious2secu god_exist
by treatment0neutral1religious2secu god_exist : sum decision

sort treatment
by treatment: tabulate decision god_exist, chi2

sort god_exist
by god_exist: tabulate decision treatment if treatment<2, chi2

gen old=0
replace old=1 if treatment>2

gen relig=0
replace relig=1 if treatment==1 | treatment==4

gen I_g_t = god_exist * treatment
gen I_g_r = god_exist * relig


logit decision relig god_ I_g_r if treatment==0 | treatment==1, r

logit decision relig age gender us christian god_ I_g_r if treatment==0 | treatment==4, r
