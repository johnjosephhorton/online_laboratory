\documentclass{elsart}
\usepackage[comma]{natbib}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{times}
\usepackage{color}
\usepackage{lscape}
\renewcommand{\baselinestretch}{1.5}
\newcommand{\bs}[1]{\boldsymbol{#1}}
\setlength{\oddsidemargin}{0in}  
\setlength{\textwidth}{6.5in}  
\setlength{\doublerulesep}{\arrayrulewidth}
\begin{document}



\subsection{Quantitative replication: Social preferences}

A central theme in experimental economics is the existence of social
(or ``other-regarding'') preferences
\citep{andreoni1990impure,fehr1999theory}.  Countless laboratory
experiments have demonstrated that many people's behaviors are
inconsistent with caring only about their own monetary payoffs.  (For a
review, see \cite{camerer2003behavioral}.)  Here we quantitatively replicated the
existence and extent of other-regarding preferences in the laboratory using MTurk.  

To compare pro-social behavior on MTurk to that which is observed in the offline laboratory, we used the
prisoner's dilemma (``PD''), the canonical game for studying
altruistic cooperation \citep{axelrod1981evolution}. We recruited $155$ subjects on MTurk and $30$ subjects at Harvard University, using the same neutrally-framed instructions, incentive-compatible design and ex-post matching procedure. To be commensurate with standard wages on MTurk, payoffs were an order of magnitude smaller on MTurk compared to the offline lab. MTurk participants received a \$0.50 ``show-up fee,'' while offline subjects received a \$5 show-up fee, and 
each subject was informed that he or she had been randomly assigned to interact with another
participant.\footnote{MTurk workers were matched exclusively with MTurk workers, and offline subjects matched exclusively with offline subjects.} They were further informed that both players would have a choice
between two options, A or B, with the following payoff structure: 

\begin{equation}
\textnormal{MTurk:\hspace{10pt}   } \bordermatrix{
    &     A        &     B  \cr
 A  & 	\$0.70, \$0.70 &  \$0, \$1.00 \cr
B  & \$1.00, \$0 & \$0.30, \$0.30 }
\textnormal{\hspace{20pt} Offline:\hspace{10pt}   } \bordermatrix{
    &     A        &     B  \cr
 A  & 	\$7, \$7 &  \$0, \$10 \cr
B  & \$10, \$0 & \$3, \$3 }
\end{equation}

where A represents cooperation, B represents defection, and, regardless
of the action of one's partner, playing B maximizes one's payoff. MTurk workers were additionally given 5 comprehension questions regarding the payoff structure, allowing us to compare subjects who were paying close attention with those who were not.\footnote{The comprehension questions were: 1) Which earns you more money: [You pick A, You pick B]. 2) Which earns the other person more money: [You pick A, You pick B]. 3) Which earns you more money: [Other person picks A, Other person picks B]. 4) Which earns the other person more money: [Other person picks A, Other person pick B]. 5) If you pick B and the other picks A, what bonus will you receive?}




In a one-shot PD, rational self-interested players should always
select B.  Consistent with a wealth of previous laboratory studies\citep{camerer2003behavioral}, however, a substantial fraction of our subjects chose A, both offline (37\%) and on MTurk (47\%). The difference between offline and online cooperation was not statistically significant (Chi$^2$ test, p=0.294), although this may have been in part due to the relatively small offline sample size ($N=30$). However, if we restrict our attention to the $N=74$ MTurk workers who correctly answered all five comprehension questions, we find that 39\% chose A, giving almost exact quantitative agreement with the offline subjects (Chi$^2$ test, p=0.811; see Figure \ref{figX2}). These results demonstrate the ability of MTurk to quantitatively reproduce behavior from the offline lab, and also emphasize the importance of using payoff comprehension questions.

 
\begin{figure}
\begin{center} 
\includegraphics[width=3.5in]{social_pref_rep}
\caption{Cooperation in a one-shot Prisonerís Dilemma is similar amongst offline laboratory subjects, MTurk workers who correctly answered five payoff comprehension questions, and all MTurk workers. Error bars indicate standard error of the mean. \bigskip \label{figX2}}
\end{center} 
\end{figure}


\subsection{Qualitative replication: Priming}
Priming is common tool in the behavioral sciences. In priming studies, stimuli unrelated to the decision task
(and which do not affect the monetary outcomes) can nonetheless
significantly alter subjects' behaviors.  Priming has attracted a
great deal of attention in psychology, and, more recently, in
experimental economics \citep{benjamin2010social}. Here we demonstrated the existence of priming effects on
MTurk. 

To do so, we recruited $189$ subjects to
play the a PD game. In addition to a \$.20 ``show-up
fee,'' subjects were further informed of the following payoff structure 
(in units of cents): 
\begin{equation}
 \bordermatrix{
    &     A        &     B  \cr
 A  & 	\$1.20, \$1.20 &  \$0.40, \$1.60 \cr
B  & \$1.60, \$0.40 & \$0.80, \$0.80 }
\end{equation}

As in the previous PD, A represents cooperation and B represents defection.. 

 Subjects were randomly assigned to either the religious prime
group ($N=90$) or a neutral prime group ($N=99$).  The religious prime
group read a Christian religious passage about the importance of
charity (Mark $10$:$17$-$23$) before playing the PD.  The neutral prime group
instead read a passage of equal length describing three species of
fish before playing the PD.  Following the PD, each subject completed a
demographic questionnaire reporting age, gender, country of residence,
and religious affiliation.  The subjects also indicated whether they had ever had an
experience which convinced them of the existence of God.  Based on
previous results using implicit primes with a non-student subject pool
\citep{shariff2007god}, we hypothesized that the religious prime would
increase cooperation, but only among subjects who believed in God.


The results are portrayed in Figure \ref{figX}.  We analyzed the data
using logistic regression with robust standard errors, with PD
decision as the dependent variable ($0$=defect, $1$=cooperate), and
prime ($0$=neutral, $1$=religion) and believer ($0$=does not believe
in God, $1$=believes in God) as independent variables, along with a
prime $\times$ believer interaction term.  We also included age, gender
($0$=female, $1$=male), country of residence ($0$=non-U.S., $1$=U.S.),
and religion ($0$=non-Christian, $1$=Christian) as control variables.
Consistent with our prediction, we found no significant main effect of
prime ($p=0.169$) or believer ($p=0.12$), but a significant positive
interaction between the two (coeff$=2.15$, $p=0.001$).  We also found a
significant main effect of gender (coeff$=0.70$, $p=0.044$), indicating that
women are more likely to cooperate, but no significant effect of age
($p=0.52$), country of residence ($p=0.657$) or religion
($p=0.54$). We demonstrated that the religious prime significantly increases
cooperation in the PD, but only among those who believe in God.  These
findings are of particular note given the mixed results of previous
studies regarding the effectiveness of implicit religious primes for
promoting cooperation \citep{benjamin2010}.  We demonstrate that the
principle of priming can be observed with MTurk and that the
effectiveness of the prime can vary systematically, depending on the
characteristics of the reader.

\begin{figure}
\begin{center} 
\includegraphics[width=3.5in]{social_pref}
\caption{Reading a religious passage significantly increases
  Prisoner's Dilemma cooperation among those who believe in God, but
  not among non-believers. Error bars indicate standard error of the mean.\bigskip \label{figX}}
\end{center} 
\end{figure}

\end{document}


\subsection{Qualitative replication: Framing}
Traditional economic models assume that individuals are fully rational
in making decisions---that people will always choose the option that
maximizes their utility, which is wholly-defined in terms of outcomes.
Therefore, decision-making should be consistent, and an individual
should make the same choice when faced with equivalent decision
problems.  However, as the watershed experiment of
\cite{tversky1981framing} (hereafter ``TK'') demonstrated, this is
not the case.  TK introduced the concept of ``framing'':
that presenting two numerically equivalent
situations with different language can lead to dramatic differences in
stated preferences.  In our current experiment, we replicated the framing effect
demonstrated by TK on MTurk.\footnote{This is the second replication of
  this result on MTurk.  Gabriele Paolacci also performed this
  experiment and reported the results on his blog,
  \href{http://experimentalturk.wordpress.com/2009/11/06/asian-disease}{http://experimentalturk.wordpress.com/2009/11/06/asian-disease}.}

In TK's canonical example, subjects read one of two hypothetical
scenarios.  Half of the subjects were given the following Problem $1$:
\begin{quote}
Imagine that the United States is preparing for the outbreak of an unusual
Asian disease, which is expected to kill $600$ people.  Two alternative
programs to combat the disease have been proposed.  Assume that the
exact scientific estimates of the consequences of the programs are as
follows: If Program A is adopted, $200$ people will be saved.  If Program
B is adopted, there is $\frac{1}{3}$ probability that $600$ people will be saved
and $\frac{2}{3}$ probability that no people will be saved.

Which of the two programs would you favor?
\end{quote}

\noindent The other half were given Problem $2$ in which the setup (the first three sentences) was
identical but the programs were framed differently:
\begin{quote}
If Program A is adopted, $400$ people will die.  If Program B is adopted,
there is $\frac{1}{3}$ probability that nobody will die, and $\frac{2}{3}$
probability that $600$ people will die.
\end{quote}

The two scenarios are numerically identical, but the subjects responded
very differently.  TK found that in Problem $1$, where the scenario was
framed in terms of gains, subjects were risk-averse: $72$\% chose the
certain Program A over the risky Program B. However, in Problem $2$,
where the scenario was framed in terms of losses, $78$\% of
subjects preferred Program B.

Using these same prompts, we recruited $213$ subjects to see whether
they would reproduce this preference reversal on MTurk.  We offered a
participation fee of US\$$0.40$.  We randomly assigned subjects to a
treatment upon arrival. Consistent with TK's results, we found that
the majority of subjects preferred Program A in the domain of gains
(N=$95$: $69$\% A, $31$\% B), while the opposite was true in the
domain of losses (N=$118$: $41$\% A, $59$\% B).  The framing
significantly affected, and in fact reversed, the pattern of
preferences stated by the subjects ($\chi^2$ test, $p<0.001$).  Thus,
we successfully replicated the principle of framing on MTurk.

