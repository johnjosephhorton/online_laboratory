clear
insheet using "C:\Documents and Settings\Dave-O\My Documents\RESEARCH PROJECTS\john horton\My Dropbox\how_to\empirical_component\lab_PD\lab_PD_replication_data.csv"

replace decision="1" if decision=="You choose A"
replace decision="0" if decision=="You choose B"
destring decision, replace

gen mturk=0
replace mturk=1 if comp5~=.

gen comped=0
replace comped=1 if comp1=="You pick B" & comp2=="You pick A" & comp3=="Other person picks A" & v9=="Other person picks B" & comp5==100
replace comped=1 if mturk==0

gen halfcomped=0
replace halfcomped=1 if comp1=="You pick B" & comp2=="You pick A" & comp3=="Other person picks A" & v9=="Other person picks B" 
replace halfcomped=1 if mturk==0

sum decision if mturk==0
sum decision if mturk==1
sort comped
by comped: sum decision if mturk==1

tabulate decision mturk, chi2
tabulate decision mturk if comped==1, chi2
tabulate decision mturk if comped==0 | mturk==0, chi2

