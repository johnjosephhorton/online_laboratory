
import os, shutil, time  

topic = "market_game" 
path = '/home/john/Dropbox/research/how_to/market_game/'
dir_name = "/tmp/market_game%s"%int(round(time.time(),0))
os.mkdir(dir_name)
os.chdir(path)
os.system('cp -r ./writeup %s'%dir_name)
os.system('cp -r ./data %s'%dir_name)
os.system('cp ../writeup/references.bib %s'%dir_name)
os.chdir(dir_name)
os.chdir('writeup')
os.system("""echo "Sweave('%s.Rnw')" | R --vanilla --quiet"""%topic)
os.system("sed -f %s.sed %s.tex > temp.tex"%(topic, topic)) 
#os.system('mv %s.tex temp.tex'%topic)
os.system('pdflatex temp')
os.system('bibtex temp')
os.system('pdflatex temp')
os.system('pdflatex temp')
shutil.copy('temp.pdf', path + "/writeup/" + topic + ".pdf")
